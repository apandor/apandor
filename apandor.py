import dbus
import sys
import select
import crcmod.predefined
import xml.etree.ElementTree as et

#xmlfile = "records.xml"
xmlfile = "pandora.xml"

# Read the service record from file
try:
    fh = open(sys.path[0] + "/" + xmlfile, "r")
except:
    sys.exit("Could not open the sdp record. " + xmlfile + "  Exiting...")
records = fh.read()
fh.close()

channels = {
    0x10000001: "Channel 1",
    0x10000002: "Channel 2",
    0x10000003: "Channel 3",
    0x10000004: "Channel 4",
}

sdp_service_xml = {}
sdp_service_port = {}

while True:
    i = records.rfind('<?xml')
    if i < 0: break
    data = records[i:]
    xml = et.fromstring(data)
    port_el = xml.findall("./attribute/[@id='0x0004']/sequence/sequence/uuid[@value='0x0003']../uint8")

    name_el = xml.findall("./attribute/[@id='0x0100']/text")
    if(len(name_el) > 0 and name_el[0].attrib['value'] != None and len(port_el) > 0 and port_el[0].attrib['value'] != None):
        name = name_el[0].attrib['value'].rstrip()
        sdp_service_xml[name] = data
        sdp_service_port[name] = int(port_el[0].attrib['value'], 0)
    
    records = records[:i]


app = "pandora"
app_service = 'PandoraLink'
#app = "aha"
#app_service = 'aha-spp-link'
#app = "stitcher"
#app_service = 'StitcherConnect'
if app_service not in sdp_service_port:
   print app + " service not found"
   sys.exit(1)

xml = sdp_service_xml[app_service]
port = sdp_service_port[app_service]

def advertise_service(sdp_record_xml):
    bus = dbus.SystemBus()
    manager = dbus.Interface(bus.get_object("org.bluez", "/"),
                             "org.bluez.Manager")
    #adapter_path = manager.FindAdapter(self.device_id)
    adapter_path = manager.DefaultAdapter()
    service = dbus.Interface(bus.get_object("org.bluez", adapter_path),
                             "org.bluez.Service")
    service.AddRecord(sdp_record_xml)

def data_escape(data):
    return data.replace("\x7d", "\x7d\x5d").replace("\x7e", "\x7d\x5e").replace("\x7c", "\x7d\x5c")

def data_unescape(data):
    return data.replace("\x7d\x5c", "\x7c").replace("\x7d\x5e", "\x7e").replace("\x7d\x5d", "\x7d")

def data_packet(ack, seq, data):
    data = chr(ack) + chr(seq) + struct.pack("!I", len(data)) + data
    return "\x7e" + data_escape(data + struct.pack("!H", crc16(data))) + "\x7c"

advertise_service(xml)
print "starting " + app + " service on port " + str(port)

from bluetooth import *

server_socket=BluetoothSocket( RFCOMM )

server_socket.bind(("", port ))
server_socket.listen(1)

client_socket, address = server_socket.accept()

print "accepted"

inputs = [ client_socket ]
outputs = [ ]

nextdata = ''

cmd_queue = []
seq = 0

crc16 = crcmod.mkCrcFun(0x11021, 0xffff, False, 0x0000)

while inputs:
    readable, writable, exceptional = select.select(inputs, outputs, inputs)
    for s in readable:
        if s is client_socket:
            print "recvC"
            data = client_socket.recv(4096)
            print "done recvC"

            if not data:
                inputs.remove(s)
                s.close
                continue

            print "received [%d]" % len(data)
            nextdata = nextdata + data;

            while nextdata != '':
                data = nextdata

                if (data[0] != "\x7e"):
                    print "invalid packet: %02x" % ord(data[0])
                    break
                i = data.find("\x7c")
                if(i < 0):
                    # incomplete packet
                    nextdata = data
                    break

                i = i+1

                nextdata = data[i:]
                data = data_unescape(data[:i])

                ack = ord(data[1])
                cliseq = ord(data[2])

                if(ack != 0):
                    print "ack"
                    if(cliseq == seq ^ 1):
                        print "good ack"
                        if(len(cmd_queue) > 0): cmd_queue = cmd_queue[1:]
                    else:
                        print "bad ack. resend %02x" % ord(cmd_queue[0][0])
                    if(len(cmd_queue) > 0):
                        seq = seq ^ 1
                        client_socket.send(data_packet(0, seq, cmd_queue[0]))
                    continue

                n = struct.unpack("!I", data[3:7])[0]
                data = data[7:7+n]

                type = ord(data[0])

                client_socket.send(data_packet(1, cliseq ^ 1, ''))

                print "command: %02x" % type

                queue_was_empty = (len(cmd_queue) == 0);

                if(type == 0x00):
                    print "connect"

                    print "update status"
                    cmd_data = "\x81\x01"
                    cmd_queue.append(cmd_data)
                elif (type == 0x41 ):
                    cmd_data = "\xb2" + chr(len(channels))
                    cmd_queue.append(cmd_data)
                elif (type == 0x42 ):
                    print "get channel id's"
                    cmd_data = "\xb3\x00"
                    for i in channels:
                       cmd_data += struct.pack("!I", i)
                    print len(cmd_data)
                    cmd_queue.append(cmd_data)

                    cmd_data = "\xba" + struct.pack("!I", 0x10000001)
#                    cmd_data = "\xba" + struct.pack("!I", 0x0)
                    cmd_queue.append(cmd_data)

                elif (type == 0x44 ):
                    print "get channel info"
                    ids = struct.unpack("!I", data[1:])
                    for id in ids:
                        cmd_data = "\xb4" + struct.pack("!I", id) + "\x00" + channels[id] + "\x00"
                        cmd_queue.append(cmd_data)
                elif (type == 0x45 ):
                    print "get channel order"

                    print "set station order"
                    cmd_data = "\xb5" + "\x01"
                    cmd_queue.append(cmd_data)
                elif (type == 0x10 ):
                    print "get track title"
                    cmd_data = "\x91" + struct.pack("!I", 1) + struct.pack("!I", 0) + struct.pack("!H", 60) +  struct.pack("!H", 0) + chr(0) + chr(1) + "Dummy Title\x00"
                    cmd_queue.append(cmd_data)
                elif (type == 0x11 ):
                    print "get track title"
                    cmd_data = "\x92" + struct.pack("!I", 1) + "Dummy Title\x00"
                    cmd_queue.append(cmd_data)
                elif (type == 0x12 ):
                    print "get track artist"
                    cmd_data = "\x93" + struct.pack("!I", 1) + "Dummy Artist\x00"
                    cmd_queue.append(cmd_data)
                elif (type == 0x13 ):
                    print "get track album"
                    cmd_data = "\x94" + struct.pack("!I", 1) + "Dummy Album\x00"
                    cmd_queue.append(cmd_data)
                elif (type == 0x15 ):
                    print "enable/disable elapsed polling"
                elif (type == 0x47 ):
                    i = struct.unpack("!I", data[1:5])[0]
                    print "select channel " + channels[i]
                elif (type == 0x30 ):
                    print "playing"
                else:
                    print "unknown command: %02x" % ord(data[0])

                if(queue_was_empty and len(cmd_queue) > 0):
                    seq = seq ^ 1
                    client_socket.send(data_packet(0, seq, cmd_queue[0]))


#client_socket.close()
server_socket.close()
