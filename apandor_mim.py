import dbus
import sys
import select
import subprocess
import re
import xml.etree.ElementTree as et

if len(sys.argv) < 2:
  sys.exit('Usage: %s BTADDR' % sys.argv[0])

btaddr = sys.argv[1]

sdptool = subprocess.Popen(["sdptool", "browse", "--xml", btaddr], stdout=subprocess.PIPE).stdout

records = ''
while True:
  data = sdptool.read()
  if data == '':
    break
  records += data

sdp_service_xml = {}
sdp_service_port = {}

while True:
    i = records.rfind('<?xml')
    if i < 0: break
    data = records[i:]
    xml = et.fromstring(data)
    port_el = xml.findall("./attribute/[@id='0x0004']/sequence/sequence/uuid[@value='0x0003']../uint8")

    name_el = xml.findall("./attribute/[@id='0x0100']/text")
    if(len(name_el) > 0 and name_el[0].attrib['value'] != None and len(port_el) > 0 and port_el[0].attrib['value'] != None):
        name = name_el[0].attrib['value'].rstrip()
        sdp_service_xml[name] = data
        sdp_service_port[name] = int(port_el[0].attrib['value'], 0)
    
#    print (xml.findall("./attribute/[@id='0x0100']")[0]).text.value
#    et.dump(xml.findall("./attribute/[@id='0x0100']")[0])
#    r.append(et.fromstring(records[i:]))
    records = records[:i]

app = "pandora"
app_service = 'PandoraLink'
#app = "aha"
#app_service = 'aha-spp-link'
#app = "stitcher"
#app_service = 'StitcherConnect'
if app_service not in sdp_service_port:
   print app + " service not found"
   sys.exit(1)

port = sdp_service_port[app_service]

def advertise_service(sdp_record_xml):
    bus = dbus.SystemBus()
    manager = dbus.Interface(bus.get_object("org.bluez", "/"),
                             "org.bluez.Manager")
    #adapter_path = manager.FindAdapter(self.device_id)
    adapter_path = manager.DefaultAdapter()
    service = dbus.Interface(bus.get_object("org.bluez", adapter_path),
                             "org.bluez.Service")
    service.AddRecord(sdp_record_xml)

advertise_service(sdp_service_xml[app_service])

print "starting " + app_service + " service on btaddr, port " + btaddr + ", ", str(port)

from bluetooth import *

server_socket=BluetoothSocket( RFCOMM )

server_socket.bind(("", port ))
server_socket.listen(1)

# Create the client socket
client_socket2=BluetoothSocket( RFCOMM )

client_socket2.connect((btaddr, port))

client_socket, address = server_socket.accept()

print "accepted"

i = 0

inputs = [ client_socket, client_socket2 ]
outputs = [ ]

while inputs:
    readable, writable, exceptional = select.select(inputs, outputs, inputs)
    for s in readable:
        i = i + 1
        if s is client_socket:
            print "recvC"
            data = client_socket.recv(4096)
            print "done recvC"

            print "received [%d]" % len(data)
            print "received [%s]" % data

            if data:
                type = "UNKNOWN"
                seq = 0x0;

                if(ord(data[0]) == 0x7e):
                    type = "DATA" if (ord(data[1]) == 0) else "ACK"
                    seq = ord(data[2])
                else:
                    type = "UNKNOWN-%02x" % ord(data[0]);

                f = open ("%s-%04d-C-%s-%02x.dat" % (app, i, type, seq), "w")
                n = f.write(data)
                f.close()

                print "sendP"
                client_socket2.send(data)
                print "done sendP"
            else:
                inputs.remove(s)
                s.close

        if s is client_socket2:
            print "recvP"
            data = client_socket2.recv(4096)
            print "done recvP"

            print "received2 [%d]" % len(data)
            print "received2 [%s]" % data
            if data:
                type = "UNKNOWN"
                seq = 0x0;

                if(ord(data[0]) == 0x7e):
                    type = "DATA" if (ord(data[1]) == 0) else "ACK"
                    seq = ord(data[2])
                else:
                    type = "UNKNOWN-%02x" % ord(data[0]);

                f = open ("%s-%04d-P-%s-%02x.dat" % (app, i, type, seq), "w")
                f.write(data)
                f.close()

                print "sendP"
                client_socket.send(data)
                print "done sendP"
            else:
                inputs.remove(s)
                s.close

#client_socket.close()
server_socket.close()
